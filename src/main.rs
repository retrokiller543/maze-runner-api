use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use csv::Writer;
use env_logger::Env;
use log::{debug, error, info, trace};
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::Mutex;
use std::any::Any;

#[derive(Serialize, Clone)]
struct InitGame {
    text: String,
    gameid: String,
}

#[derive(Deserialize, Debug)]
struct QueryParams {
    #[serde(default)]
    r#type: String,
}

#[derive(Clone, Debug)]
struct GameState {
    map: String,
    current_room: u32,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Maze {
    rooms: HashMap<String, Room>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Room {
    roomid: u32,
    description: String,
    directions: Directions,
}

#[derive(Serialize, Debug, Clone)]
struct RoomForCsv {
    roomid: u32,
    description: String,
    west: String,
    east: String,
    south: String,
    north: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(untagged)]
enum DirectionValue {
    Int(u32),
    Str(String),
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Directions {
    west: DirectionValue,
    east: DirectionValue,
    south: DirectionValue,
    north: DirectionValue,
}

fn get_game_state(
    game_states: &Mutex<HashMap<String, GameState>>,
    game_id: &str,
) -> Result<GameState, HttpResponse> {
    match game_states.lock() {
        Ok(game_states) => {
            if let Some(game_state) = game_states.get(game_id) {
                Ok(game_state.clone())
            } else {
                Err(HttpResponse::NotFound().finish())
            }
        }
        Err(e) => {
            error!("Failed to lock game states: {}", e);
            Err(HttpResponse::InternalServerError().finish())
        }
    }
}

fn get_current_room(game_state: &GameState) -> Result<(Maze, Room), HttpResponse> {
    let maze = match Maze::from_json(&game_state.map) {
        Ok(maze) => maze,
        Err(e) => {
            error!("Failed to parse map: {}", e);
            return Err(HttpResponse::InternalServerError().finish());
        }
    };

    match maze.get_room(game_state.current_room) {
        Some(room) => Ok((maze.clone(), room.clone())),
        None => {
            error!("Room ID {} not found", game_state.current_room);
            Err(HttpResponse::NotFound().finish())
        }
    }
}

fn get_maze_from_state(game_state: &GameState) -> Result<Maze, HttpResponse> {
    match Maze::from_json(&game_state.map) {
        Ok(maze) => Ok(maze),
        Err(e) => {
            error!("Failed to parse map: {}", e);
            Err(HttpResponse::InternalServerError().finish())
        }
    }
}

#[allow(dead_code)]
impl Maze {
    // Create a new empty Maze
    pub fn new() -> Self {
        Self {
            rooms: HashMap::new(),
        }
    }

    // Load a Maze from JSON data
    pub fn from_json(json_data: &str) -> Result<Self, serde_json::Error> {
        let rooms: HashMap<String, Room> = serde_json::from_str(json_data)?;
        Ok(Self { rooms })
    }

    // Add a room to the Maze
    pub fn add_room(&mut self, room: Room) {
        self.rooms.insert(room.roomid.to_string(), room);
    }

    // Get a room by its ID
    pub fn get_room(&self, roomid: u32) -> Option<&Room> {
        self.rooms.get(&roomid.to_string())
    }
}

fn format_response<T: serde::Serialize + 'static>(data: T, query: &QueryParams) -> HttpResponse {
    let any_data = &data as &dyn Any;

    if query.r#type == "csv" {
        let mut buffer = Vec::new();
        {
            let mut csv_writer = Writer::from_writer(&mut buffer);

            // check if data is of type room
            if any_data.is::<Room>() {
                let room = any_data.downcast_ref::<Room>().unwrap();
                let room = room.to_csv();
                if let Err(e) = csv_writer.serialize(room) {
                    error!("Failed to serialize data: {}", e);
                    return HttpResponse::InternalServerError().finish();
                }
            } else {
                let data = data;
                if let Err(e) = csv_writer.serialize(data) {
                    error!("Failed to serialize data: {}", e);
                    return HttpResponse::InternalServerError().finish();
                }
            }
        }
        debug!("CSV data: {:?}", String::from_utf8_lossy(&buffer));
        HttpResponse::Ok().content_type("text/csv").body(buffer)
    } else {
        HttpResponse::Ok().json(data) // JSON response
    }
}

impl Room {
    pub fn to_csv(&self) -> RoomForCsv {
        RoomForCsv {
            roomid: self.roomid,
            description: self.description.clone(),
            west: match &self.directions.west {
                DirectionValue::Int(val) => val.to_string(),
                DirectionValue::Str(val) => val.clone(),
            },
            east: match &self.directions.east {
                DirectionValue::Int(val) => val.to_string(),
                DirectionValue::Str(val) => val.clone(),
            },
            south: match &self.directions.south {
                DirectionValue::Int(val) => val.to_string(),
                DirectionValue::Str(val) => val.clone(),
            },
            north: match &self.directions.north {
                DirectionValue::Int(val) => val.to_string(),
                DirectionValue::Str(val) => val.clone(),
            },
        }
    }
}


#[get("/")]
async fn index(
    game_states: web::Data<Mutex<HashMap<String, GameState>>>,
    query: web::Query<QueryParams>,
) -> impl Responder {
    debug!("Starting new Game...");
    debug!("Query: {:?}", &query);

    let mut rng = rand::thread_rng();
    let new_game_id: u16 = rng.gen();

    let new_game_id_str = new_game_id.to_string();

    let mut game_states = match game_states.lock() {
        Ok(game_states) => game_states,
        Err(e) => {
            error!("Failed to lock game states: {}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };
    game_states.insert(
        new_game_id_str.clone(),
        GameState {
            map: String::new(),
            current_room: 0,
        },
    );

    info!("Game started successfully with ID: {}", &new_game_id_str);

    let init_game = InitGame {
        text: String::from("New Game Started!"),
        gameid: new_game_id_str,
    };

    format_response(init_game, &query)
}

#[get("maps")]
async fn get_maps(query: web::Query<QueryParams>) -> impl Responder {
    trace!("Retriving maps...");
    debug!("Query: {:?}", &query);

    let maps_dir = match std::fs::read_dir("./maps") {
        Ok(maps_dir) => maps_dir,
        Err(e) => {
            error!("Failed to read maps directory: {}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };
    let mut maps: Vec<String> = Vec::new();

    for map in maps_dir {
        let map = match map {
            Ok(map) => map,
            Err(e) => {
                error!("Failed to read map: {}", e);
                return HttpResponse::InternalServerError().finish();
            }
        };
        let map_name = match map.file_name().into_string() {
            Ok(map_name) => map_name,
            Err(e) => {
                error!("Failed to convert map name to string: {:?}", e);
                return HttpResponse::InternalServerError().finish();
            }
        };
        maps.push(map_name);
    }

    info!("Maps (indexed from 0): {:?}", maps);

    format_response(maps, &query)
}

#[get("{game_id}/map/{map_name}")]
async fn set_map(
    game_states: web::Data<Mutex<HashMap<String, GameState>>>,
    path_info: web::Path<(String, String)>,
    query: web::Query<QueryParams>,
) -> impl Responder {
    let (game_id, map_name) = path_info.into_inner();

    debug!("Setting map...");
    debug!("Query: {:?}", &query);
    debug!("Game ID: {}", &game_id);
    debug!("Map Name: {}", &map_name);

    let mut map_path = format!("./maps/{}", &map_name);

    if !map_name.ends_with(".json") {
        map_path.push_str(".json");
    }

    let map = match std::fs::read_to_string(&map_path) {
        Ok(map) => map,
        Err(e) => {
            error!("Failed to read map: {}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };

    {
        let mut game_states = game_states.lock().unwrap();
        if let Some(game_state) = game_states.get_mut(&game_id) {
            game_state.map = map.clone();
        } else {
            error!("Game ID {} not found", &game_id);
            return HttpResponse::NotFound().finish();
        }
    }

    info!("Map set successfully to {}!", &map_name);

    let response = format!("Map set successfully to {}", &map_name);

    format_response(response, &query)
}

#[get("{game_id}/maze")]
async fn get_maze(
    game_states: web::Data<Mutex<HashMap<String, GameState>>>,
    path_info: web::Path<String>,
    query: web::Query<QueryParams>,
) -> impl Responder {
    let game_id = path_info.into_inner();

    debug!("Getting maze...");
    debug!("Query: {:?}", &query);
    debug!("Game ID: {}", &game_id);

    let game_state = match get_game_state(&game_states, &game_id) {
        Ok(game_state) => game_state,
        Err(response) => return response,
    };

    let maze = match get_maze_from_state(&game_state) {
        Ok(maze) => maze,
        Err(response) => return response,
    };

    let current_room = match maze.get_room(game_state.current_room) {
        Some(current_room) => current_room.clone(),
        None => {
            error!("Room ID {} not found", &game_state.current_room);
            return HttpResponse::NotFound().finish();
        }
    };

    format_response(current_room, &query)
}

#[get("{game_id}/maze/{room_id}")]
async fn get_room_info(
    game_states: web::Data<Mutex<HashMap<String, GameState>>>,
    path_info: web::Path<(String, u32)>,
    query: web::Query<QueryParams>,
) -> impl Responder {
    let (game_id, room_id) = path_info.into_inner();

    debug!("Getting room info...");
    debug!("Query: {:?}", &query);
    debug!("Game ID: {}", &game_id);
    debug!("Room ID: {}", &room_id);

    let game_state = match get_game_state(&game_states, &game_id) {
        Ok(game_state) => game_state,
        Err(response) => return response,
    };

    let maze = match get_maze_from_state(&game_state) {
        Ok(maze) => maze,
        Err(response) => return response,
    };

    let room = match maze.get_room(room_id) {
        Some(room) => room.clone(),
        None => {
            error!("Room ID {} not found", &room_id);
            return HttpResponse::NotFound().finish();
        }
    };

    format_response(room, &query)
}

#[get("{game_id}/maze/move/{direction}")]
async fn move_direction(
    game_states: web::Data<Mutex<HashMap<String, GameState>>>,
    path_info: web::Path<(String, String)>,
    query: web::Query<QueryParams>,
) -> impl Responder {
    let (game_id, direction) = path_info.into_inner();

    debug!("Moving in direction...");
    debug!("Query: {:?}", &query);
    debug!("Game ID: {}", &game_id);
    debug!("Direction: {}", &direction);

    let game_state = match get_game_state(&game_states, &game_id) {
        Ok(game_state) => game_state,
        Err(response) => return response,
    };

    let current_room = match get_current_room(&game_state) {
        Ok((_, current_room)) => current_room,
        Err(response) => return response,
    };

    let next_room_id = match direction.as_str() {
        "west" => match &current_room.directions.west {
            DirectionValue::Int(room_id) => room_id,
            DirectionValue::Str(_) => {
                error!("Invalid direction: {}", &direction);
                return HttpResponse::BadRequest().finish();
            }
        },
        "east" => match &current_room.directions.east {
            DirectionValue::Int(room_id) => room_id,
            DirectionValue::Str(_) => {
                error!("Invalid direction: {}", &direction);
                return HttpResponse::BadRequest().finish();
            }
        },
        "south" => match &current_room.directions.south {
            DirectionValue::Int(room_id) => room_id,
            DirectionValue::Str(_) => {
                error!("Invalid direction: {}", &direction);
                return HttpResponse::BadRequest().finish();
            }
        },
        "north" => match &current_room.directions.north {
            DirectionValue::Int(room_id) => room_id,
            DirectionValue::Str(_) => {
                error!("Invalid direction: {}", &direction);
                return HttpResponse::BadRequest().finish();
            }
        },
        _ => {
            error!("Invalid direction: {}", &direction);
            return HttpResponse::BadRequest().finish();
        }
    };

    info!("Next room ID: {}", &next_room_id);
    debug!("Current room ID: {}", &game_state.current_room);

    let mut game_states_locked = match game_states.lock() {
        Ok(guard) => guard,
        Err(_) => {
            error!("Failed to lock game states.");
            return HttpResponse::InternalServerError().finish();
        }
    };

    if let Some(game_state) = game_states_locked.get_mut(&game_id) {
        game_state.current_room = *next_room_id;
        debug!("New current room ID: {}", game_state.current_room);

        // Fetch the new current room after moving
        let new_current_room = match get_current_room(game_state) {
            Ok((_, current_room)) => current_room,
            Err(response) => return response,
        };

        // Respond with the new current room
        format_response(new_current_room, &query)
    } else {
        error!("Game ID {} not found", &game_id);
        HttpResponse::NotFound().finish()
    }
}

fn setup_logging() {
    #[cfg(debug_assertions)]
    let env = Env::default()
        .filter_or("LOG_LEVEL", "debug")
        .write_style_or("LOG_STYLE", "always");

    #[cfg(not(debug_assertions))]
    let env = Env::default()
        .filter_or("LOG_LEVEL", "info")
        .write_style_or("LOG_STYLE", "always");

    env_logger::init_from_env(env);
    trace!("Logging is initialized.");
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    setup_logging();
    info!("Starting Maze API...");

    let game_states = web::Data::new(
        Mutex::new(HashMap::<String, GameState>::new())
    );

    HttpServer::new(move || {
        App::new()
            .app_data(game_states.clone())
            .service(index)
            .service(get_maps)
            .service(set_map)
            .service(get_maze)
            .service(get_room_info)
            .service(move_direction)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
