
# Maze API Documentation

## Table of Contents
1. [Introduction](#introduction)
2. [Base URL](#base-url)
3. [Endpoints](#endpoints)
   - [Initialize Game](#initialize-game)
   - [List Maps](#list-maps)
   - [Set Map](#set-map)
   - [Get Maze](#get-maze)
   - [Get Room Info](#get-room-info)
   - [Move Direction](#move-direction)
4. [Data Models](#data-models)
5. [Errors](#errors)

## Introduction
This document describes the API for a Maze game. The API is built using Rust and Actix Web and provides functionalities to manage game states, mazes, rooms, and directions.

## Base URL
All API requests are made to:
```
http://127.0.0.1:8080/
```

## Endpoints

### Initialize Game
- **Path**: `/`
- **Method**: `GET`
- **Query Parameters**:
    - `type`: Output format (`json` or `csv`)
- **Response**: A JSON object containing the new game ID and a text message.
- **Status Codes**:
    - `200 OK`: Successful
    - `500 Internal Server Error`: Server error

### List Maps
- **Path**: `/maps`
- **Method**: `GET`
- **Query Parameters**:
    - `type`: Output format (`json` or `csv`)
- **Response**: A list of available maps.
- **Status Codes**:
    - `200 OK`: Successful
    - `500 Internal Server Error`: Server error

### Set Map
- **Path**: `/{game_id}/map/{map_name}`
- **Method**: `GET`
- **Path Parameters**:
    - `game_id`: Game ID
    - `map_name`: Map name
- **Query Parameters**:
    - `type`: Output format (`json` or `csv`)
- **Response**: Confirmation text.
- **Status Codes**:
    - `200 OK`: Successful
    - `404 Not Found`: Game ID not found
    - `500 Internal Server Error`: Server error

### Get Maze
- **Path**: `/{game_id}/maze`
- **Method**: `GET`
- **Path Parameters**:
    - `game_id`: Game ID
- **Query Parameters**:
    - `type`: Output format (`json` or `csv`)
- **Response**: JSON object representing the current room.
- **Status Codes**:
    - `200 OK`: Successful
    - `404 Not Found`: Game ID or room not found
    - `500 Internal Server Error`: Server error

### Get Room Info
- **Path**: `/{game_id}/maze/{room_id}`
- **Method**: `GET`
- **Path Parameters**:
    - `game_id`: Game ID
    - `room_id`: Room ID
- **Query Parameters**:
    - `type`: Output format (`json` or `csv`)
- **Response**: JSON object representing the room.
- **Status Codes**:
    - `200 OK`: Successful
    - `404 Not Found`: Game ID or room not found
    - `500 Internal Server Error`: Server error

### Move Direction
- **Path**: `/{game_id}/maze/move/{direction}`
- **Method**: `GET`
- **Path Parameters**:
    - `game_id`: Game ID
    - `direction`: Direction (`west`, `east`, `south`, `north`)
- **Query Parameters**:
    - `type`: Output format (`json` or `csv`)
- **Response**: JSON object representing the new current room after moving.
- **Status Codes**:
    - `200 OK`: Successful
    - `400 Bad Request`: Invalid direction
    - `404 Not Found`: Game ID or room not found
    - `500 Internal Server Error`: Server error

## Data Models

### InitGame
- `text`: String
- `game_id`: String

### QueryParams
- `type`: String (default is empty)

### GameState
- `map`: String
- `current_room`: u32

### Maze
- `rooms`: HashMap<String, Room>

### Room
- `roomid`: u32
- `description`: String
- `directions`: Directions

### Directions
- `west`: DirectionValue
- `east`: DirectionValue
- `south`: DirectionValue
- `north`: DirectionValue

### DirectionValue
- `Int(u32)`
- `Str(String)`

## Errors

- `400 Bad Request`: Invalid query or path parameters.
- `404 Not Found`: The requested resource was not found.
- `500 Internal Server Error`: An error occurred on the server.

