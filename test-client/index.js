const axios = require('axios');

const BASE_URL = 'http://localhost:8080';

async function initGame() {
    try {
        const response = await axios.get(`${BASE_URL}/`);
        const gameId = response.data.gameid;
        console.log(`New game initialized with game ID: ${gameId}`);
        return gameId;
    } catch (error) {
        console.error(`Failed to initialize game: ${error}`);
        return null;
    }
}

async function getMaps() {
    try {
        const response = await axios.get(`${BASE_URL}/maps`);
        const maps = response.data;
        console.log(`Available maps: ${maps}`);
        return maps;
    } catch (error) {
        console.error(`Failed to get maps: ${error}`);
        return null;
    }
}

async function selectMap(gameId, mapName) {
    try {
        await axios.get(`${BASE_URL}/${gameId}/map/${mapName}`);
        console.log(`Map ${mapName} selected for game ID ${gameId}`);
    } catch (error) {
        console.error(`Failed to select map: ${error}`);
    }
}

async function enterMaze(gameId) {
    try {
        const response = await axios.get(`${BASE_URL}/${gameId}/maze?type=csv`);
        const roomInfo = response.data;
        console.log(`Entered the maze, current room info: ${JSON.stringify(roomInfo)}`);
    } catch (error) {
        console.error(`Failed to enter maze: ${error}`);
    }
}

(async () => {
    const gameId = await initGame();
    if (gameId !== null) {
        const maps = await getMaps();
        if (maps !== null && maps.length > 0) {
            await selectMap(gameId, maps[0]);
            await enterMaze(gameId);
        }
    }
})();