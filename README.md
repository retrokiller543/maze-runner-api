Maze API Reference
==================================

Response Type
----------------------------------
The default response type is JSON.

You can change the response type to CSV (comma-separated values) by adding the query string `?type=csv`.

GET /
----------------------------------
Initialize the maze and generates a new game ID.

*Example call*

```
/
```

### Parameters

| Name | Required | Example Value | Description             |
|------|----------|---------------|-------------------------|
| type | No       | csv           | Preferred response type |

*JSON response*

```json
{
    "text": "New game initialized",
    "gameid": "42580"
}
```

GET /maps
----------------------------------
Returns a list of all available maps.

*Example call*

```
/maps
```

### Parameters

| Name | Required | Example Value | Description             |
|------|----------|---------------|-------------------------|
| type | No       | csv           | Preferred response type |

*JSON response*

```json
[
    "maze-of-doom.json",
    "small-maze.json"
]
```

GET /:gameid/map/:map
----------------------------------
Loads the map as the current maze.

*Example call*

```
/42580/map/maze-of-doom.json
/42580/map/maze-of-doom
```

### Parameters

| Name   | Required | Example Value | Description                            |
|--------|----------|---------------|----------------------------------------|
| gameid | Yes      | 42580         | The numerical ID of the game           |
| map    | Yes      | maze-of-doom  | The maze to be loaded into the game    |
| type   | No       | csv           | Preferred response type                |

*JSON response*

```json
{
    "text": "New map selected."
}
```

GET /:gameid/maze
----------------------------------
Enters the maze and gets the content of the first room.

*Example call*

```
/42580/maze
```

### Parameters

| Name   | Required | Example Value | Description               |
|--------|----------|---------------|---------------------------|
| gameid | Yes      | 42580         | The numerical ID of the game |
| type   | No       | csv           | Preferred response type   |

*JSON response*

```json
{
  "roomid": 0,
  "description": "You are in a dark room",
  "directions": {
    "west": "-",
    "east": 1,
    "south": 3,
    "north": "-"
  }
}
```

GET /:gameid/maze/:roomid
----------------------------------
Gets info about the room.

*Example call*

```
/42580/maze/5
```

### Parameters

| Name   | Required | Example Value | Description               |
|--------|----------|---------------|---------------------------|
| gameid | Yes      | 51342         | The numerical ID of the game |
| roomid | Yes      | 5             | The numerical ID of the room |
| type   | No       | csv           | Preferred response type   |

*JSON response*

```json
{
  "roomid": 5,
  "description": "Room 5",
  "directions": {
    "west": "-",
    "east": "-",
    "south": 8,
    "north": 2
  }
}
```

GET /:gameid/maze/move/:direction
----------------------------------
Walks away from the current room in the selected direction.

*Example call*

```
/42580/maze/5/south
```

### Parameters

| Name      | Required | Example Value | Description                |
|-----------|----------|---------------|----------------------------|
| gameid    | Yes      | 33333         | The numerical ID of the game  |
| direction | Yes      | south         | The direction to move next |
| type      | No       | csv           | Preferred response type    |

*JSON response*

```json
{
  "roomid": 8,
  "description": "Room 8",
  "directions": {
    "west": 7,
    "east": "-",
    "south": 11,
    "north": 5
  }
}
```

Revision History
----------------------------------
2023-10-19 (ChatGPT) Initial draft crafted.
